/* parralax effect */

$('.main-screen').parallax({imageSrc: '../img/main-screen/1.jpg'});

$('.feedback').parallax({imageSrc: '../img/feedback/2.jpg'});

$('.results').parallax({imageSrc: '../img/results/3.jpg'});

$('.consultation').parallax({imageSrc: '../img/consultation/bg-consultation.png'});

/* init and custom slick slider */

$(document).ready(function(){
  $('.results__slider').slick({
    dots: false,
    waitForAnimate: false,
    pauseOnFocus: false,
    pauseOnHover: false,
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    prevArrow: '<img src="../img/results/left.png">',
    nextArrow: '<img src="../img/results/right.png">',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true
        }
      }
    ]
  });
});

/* init wow lib */
new WOW().init();

/* init mask phone */
$(function($){
  $("#phone").mask("+7 (999) 999-99-99");
});

/* yandex maps */
ymaps.ready(init);
var myMap,
    myPlacemark;

function init(){
  myMap = new ymaps.Map("map", {
      center: [51.538322, 46.025460],
      zoom: 17
  });

  myPlacemark = new ymaps.Placemark([51.538122, 46.025660], {
      hintContent: 'Адвокат Сотников Е.А.',
      balloonContent: 'ул. Кутякова 64А'
  }, {
      // Опции.
      // Необходимо указать данный тип макета.
      iconLayout: 'default#image',
      // Своё изображение иконки метки.
      iconImageHref: '../../img/contacts/dots-green.png',
      // Размеры метки.
      iconImageSize: [31, 40],
      // Смещение левого верхнего угла иконки относительно
      // её "ножки" (точки привязки).
      iconImageOffset: [-16, -40]
  }),
  myMap.behaviors.disable('scrollZoom');
  myMap.geoObjects.add(myPlacemark);
}

/* lazy scrool */
$(function(){
  $('.menu__link').click(function(){
    var id = $(this).attr('href');

    $('html, body').animate({
      scrollTop: ($(id).offset().top - 75)
    }, 500);
    return false;
  });
});


$(function(){
  $('.nav-item').click(function(){
    var id = $(this).attr('href');

    $('html, body').animate({
      scrollTop: ($(id).offset().top)
    }, 500);
    return false;
  });
});

/* buttom to top */
$(function() {
  $(window).scroll(function() {
    if($(this).scrollTop() > 500) {
      $('#toTop').fadeIn();
    } else {
      $('#toTop').fadeOut();
    }
  });
  $('#toTop').click(function() {
    $('body,html').animate({scrollTop:0},800);
  });
});
